import http from "http"
import socketApi from "./index"
import express from "express"
import cors from "cors"

var app = express();
app.use(cors());
app.use(express.json());

var port = process.env.PORT || "3001";
app.set("port", port);

var server = http.createServer(app);

var io = socketApi.io;
io.attach(server);

server.listen(port);

export { server }