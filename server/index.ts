import logger from "../src/util/logger"
import Event from "../src/domain/SocketEvent"
import socket_io, { Socket } from "socket.io"
import { DeviceFingerprint } from "../src/domain/DeviceFingerprint";
import { RoomSettings } from "../src/domain/RoomSettings";
import { Sketch } from "../src/domain/Sketch";

var io = socket_io();
export default {
  io: io,
  sendNotification: function() {
    io.sockets.emit('hello', {msg: 'Hello World!'});
  }
};

const log = logger({
  label: "Server",
});

const socketLog = (socket) => (msg: any) => {
  const message = JSON.stringify(msg);
  socket.send(message)
  log.info(message)
}

declare module "socket.io" {
  export interface Socket {
    info(msg: any);
  }
}

const roomsSettings: Map<string, RoomSettings> = new Map();

io.on("connection", (socket: Socket) => {
  let device_fingerprint: DeviceFingerprint;

  socket.info = socketLog(socket)

  socket.on(Event.query.room_list, () => {
    socket.emit(Event.query.room_list, Object.values(roomsSettings))
  })

  socket.on(Event.query.device_fingerprint, (fp: DeviceFingerprint) => {
    device_fingerprint = fp
    let room: RoomSettings;
    if (roomsSettings.has(fp.hash)) {
      room = roomsSettings.get(fp.hash);
      socket.info(`Found your Room(${room.id}). Join.`)
      socket.join(room.owner.hash)
    } else {
      socket.info("Not found your room. Create new")
      room = new RoomSettings()
      room.owner = fp
      room.id = fp.hash
      roomsSettings.set(fp.hash, room)
    }
    // Just send target room's settings 
    // instead of update room on new peer
    socket.emit(Event.query.device_fingerprint, room)
  })

  socket.on(Event.response.room_update, (room: RoomSettings) => {
    socket.info("UPDroom "+room.id)
    updateLocalRoom(room)
    socket.to(room.id).send(`Room(${room.id}) update`)
    socket.to(room.id).emit(Event.response.room_update, room)
  })

  socket.on(Event.query.room_join, roomId => {
    socket.join(roomId)
    const room = findRoomSettings(roomId)
    const socketRoom = socket.adapter.rooms[roomId]
    socket.info(`Joining to '${roomId}' room with ${socketRoom ? socketRoom.length-1 : 0} members`)
    socket.emit(Event.query.room_join, room)
  })

  socket.on(Event.request.sketch_hush, (room: RoomSettings) => {
    if (room) {
      socket.to(room.id).send(`Perform hush from '${device_fingerprint.components["userAgent"]}'`)
      socket.to(room.id).emit(Event.request.sketch_hush)
    }
  })
})


const findRoomSettings = (roomId: string) => {
  if (roomsSettings.has(roomId)) {
    return roomsSettings.get(roomId)
  } else {
    log.error(`Missing settings Room(${roomId})`)
    return null
  }
}

const updateLocalRoom = (room: RoomSettings) => {
  const localRoom = findRoomSettings(room.id)
  if (localRoom) {
    roomsSettings.set(room.id, room)
  }
}
