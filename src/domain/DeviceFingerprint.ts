import Fingerprint2 from "fingerprintjs2";

export class DeviceFingerprint {
  private constructor(public hash: string, public components: object) {}

  public static async create(seed: any): Promise<DeviceFingerprint> {
    return new DeviceFingerprint(
      await DeviceFingerprint.hash(seed),
      await DeviceFingerprint.get()
    );
  }

  static async get(): Promise<Map<string, any>> {
    return await Fingerprint2.getPromise({}).then((values) => {
      return Object.fromEntries(
        values.map((entry) => [entry["key"], entry["value"]])
      );
    });
  }

  static async hash(seed: any): Promise<string> {
    return Fingerprint2.getPromise({}).then((components) => {
      let values = components.map((component) => component.value);
      return Fingerprint2.x64hash128(values.join(""), seed);
    });
  }
}
