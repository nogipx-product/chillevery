import { Sketch } from "./Sketch"
import { DeviceFingerprint } from "./DeviceFingerprint"

export class RoomSettings {
  public id: string
  public sketch: Sketch
  public owner: DeviceFingerprint
}
