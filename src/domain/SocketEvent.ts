export default {
  // Events that not expect a result
  // One-way event from client
  request: {
    // Stop app instances in room
    sketch_hush: "sketch_hush",
    
    
  },

  // Events that only expect a result
  // One-way event from server
  response: {
    // React to updates of particular room
    room_update: "room_update",
  },

  // Events that expect a result
  // Two-way event from client and expect result
  query: {
    // Request a list of available rooms to connect
    room_list: "rooms_list",

    // Sends a device fingerprint and receives targer room id
    device_fingerprint: "device_fingerprint",

    // Joining some room and get its settings
    room_join: "join_room",
  }
}