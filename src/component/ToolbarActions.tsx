import React, { useContext } from "react";
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  useTheme,
} from "@material-ui/core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDice } from "@fortawesome/free-solid-svg-icons";
import { SketchBlocContext, ClientSocketBlocContext } from "../App";

export interface ToolbarActions {}

const ToolbarActions: React.FunctionComponent<ToolbarActions> = () => {
  const sketchBloc = useContext(SketchBlocContext);
  const socketBloc = useContext(ClientSocketBlocContext);
  const theme = useTheme();

  const contrast = theme.palette.getContrastText(
    theme.palette.background.default
  );

  return (
    <List>
    </List>
  );
};

export default ToolbarActions;
