import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import { ChevronLeft, ChevronRight, PowerOffSharp } from "@material-ui/icons";
import { SwipeableDrawer, AppBar, Toolbar, Drawer } from "@material-ui/core";
import logger from "../../util/logger";
import { Observable, BehaviorSubject, Subject } from "rxjs";

const log = logger({
  label: "PersistentDrawer",
});

export const useStyles = ({
  openWidth,
  closeWidth,
  backgroundColor,
  underAppBar,
}) =>
  makeStyles((theme: Theme) => {
    return createStyles({
      root: {
        display: "flex",
      },
      menuButton: {
        alignContent: "center",
        color: theme.palette.getContrastText(backgroundColor),
      },
      drawer: {
        width: openWidth,
        flexShrink: 0,
        whiteSpace: "nowrap",
        zIndex: underAppBar ? theme.zIndex.appBar - 1 : theme.zIndex.appBar + 1,
      },
      drawerOpen: {
        width: openWidth,
        transition: theme.transitions.create("width", {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }),
      },
      drawerClose: {
        transition: theme.transitions.create("width", {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        width: closeWidth,
        overflowX: "hidden",
      },
      toolbar: {
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
      },
      transparency: {
        background: "transparent",
        backgroundColor: backgroundColor,
      },
    });
  });

export interface PersistentDrawerProps {
  openWidth?: number;
  closeWidth?: number;
  title?: string;
  backgroundColor?: string;
  open?: boolean; // User side open state
  miniVariant?: boolean;
  underAppBar?: boolean;
  style?: any;
  onClose?: Function;
  onOpen?: Function;
  openSubject: Subject<boolean>;
}

export const PersistentDrawer: React.FunctionComponent<PersistentDrawerProps> = (
  props
) => {
  const classes = useStyles({
    openWidth: props.openWidth || 240,
    backgroundColor: props.backgroundColor || "#fff",
    closeWidth: props.miniVariant ? 60 : props.closeWidth || 0,
    underAppBar: props.underAppBar || false,
  })();

  const handleOpenState = (newState: boolean) => {
    if (newState && props.onOpen) {
      props.onOpen();
    }
    if (!newState && props.onClose) {
      props.onClose();
    }
  };

  // User-side state (open/closed)
  const [open, setOpen] = useState(props.open);
  useEffect(() => {
    if (props.openSubject)
      props.openSubject.subscribe((value) => {
        setOpen(value);
        handleOpenState(value);
      });
  }, [props.openSubject, open]);

  return (
    <div className={classes.root} style={props.style}>
      <CssBaseline />
      <SwipeableDrawer
        onClose={() => {}}
        onOpen={() => {}}
        open={open || props.miniVariant} // Actually drawer displaying
        variant="persistent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx(classes.transparency, classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={() => {
              if (props.openSubject) props.openSubject.next(!open);
            }}
            className={clsx(classes.menuButton)}
          >
            {open ? <ChevronLeft /> : <ChevronRight />}
          </IconButton>
        </div>
        <Divider />
        {props.children}
      </SwipeableDrawer>
    </div>
  );
};

export default PersistentDrawer;
