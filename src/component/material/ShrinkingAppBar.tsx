import React from "react";
import clsx from "clsx";
import {
  useTheme,
  createStyles,
  makeStyles,
  Theme,
} from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";


export const useStyles = ({
  shrinkWidth, backgroundColor
}) => makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      height: theme.mixins.toolbar.height,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      color: theme.palette.getContrastText(backgroundColor)
    },
    appBarShift: {
      marginLeft: shrinkWidth,
      width: `calc(100% - ${shrinkWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    transparency: {
      background: "transparent",
      backgroundColor: backgroundColor,
    },
  })
);

export interface ShrinkingAppBarProps {
  shrinkWidth?: number;
  backgroundColor?: string;
}

const ShrinkingAppBar: React.FunctionComponent<ShrinkingAppBarProps> = (
  props
) => {
  const classes = useStyles({
    shrinkWidth: props.shrinkWidth || 0,
    backgroundColor: props.backgroundColor || "#fff"
  })();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        elevation={0}
        position="fixed"
        className={clsx(classes.transparency, classes.appBar, {
          [classes.appBarShift]: props.shrinkWidth,
        })}
      >
        <Toolbar className={clsx(classes.appBar)}>
          {props.children}
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default ShrinkingAppBar;
