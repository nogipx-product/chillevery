import React, { useEffect, useRef, useState } from "react";
import { HydraController } from "../../controller/HydraController";

import assert from "assert";
import logger from "../../util/logger";

const log = logger({
  label: "UiOverlay",
});

export interface HydraCanvasProps {
  enabled?: boolean;
  onCanvasReady?: (canvas: HTMLCanvasElement) => any;
  qualityCoefficient?: number;
}

const HydraCanvas: React.FunctionComponent<HydraCanvasProps> = (props) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    let canvas = canvasRef.current;
    canvas.width = window.innerWidth * (props.qualityCoefficient || 1);
    canvas.height = window.innerHeight * (props.qualityCoefficient || 1);
    canvas.style.imageRendering = "crisp-edges";
    if (props.enabled || props.enabled === undefined) {
      props.onCanvasReady(canvas);
    }
  }, [canvasRef]);

  return (
    <div
      id="hydra-ui"
      style={{
        position: "fixed",
        left: 0,
        top: 0,
        width: "100%",
        height: "100%",
        zIndex: -1
      }}
    >
      <canvas
        id="hydra-canvas"
        ref={canvasRef}
        style={{ width: "100%", height: "100%" }}
      ></canvas>
    </div>
  );
};

export default HydraCanvas;
