import React, { useRef, useEffect, useContext } from "react";
import CodeMirror from "codemirror";
import { Sketch } from "../../domain/Sketch";
import { Observable } from "rxjs";
import { useTheme } from "@material-ui/core";
import { SketchBlocContext } from "../../App";

export interface HydraEditorProps {
  theme?: string;
  onSave?: (string) => any;
  onInit?: (CodeMirror) => any;
}

const HydraEditor: React.FunctionComponent<HydraEditorProps> = (props) => {
  const editorRef = useRef<HTMLTextAreaElement>(null);
  const sketchBloc = useContext(SketchBlocContext);
  const theme = useTheme();
  let editor: CodeMirror;

  useEffect(() => {
    editor = CodeMirror.fromTextArea(editorRef.current, {
      theme: props.theme || "monokai",
      mode: { name: "javascript", globalVars: true },
      lineWrapping: true,
      styleSelectedText: true,
    });
    if (props.onInit) props.onInit(editor);
  }, [editorRef]);

  const editorHeight =
    window.innerHeight - (theme.mixins.toolbar.height as number);

  return (
    <div style={{ height: `${editorHeight}px` }} id="hydra-editor">
      <textarea ref={editorRef}></textarea>
    </div>
  );
};

export default HydraEditor;
