import { useTheme, makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import React, { useState, useEffect, FunctionComponent } from "react";
import PersistentDrawer from "./material/PersistentDrawer";
import logger from "../util/logger";
import { Subject } from 'rxjs'
import ShrinkingAppBar from "./material/ShrinkingAppBar";
import clsx from "clsx";
import { Typography } from "@material-ui/core";

export interface OverlayProps {
  backgroundColor?: string,
  sideActions?,
  toolbarActions?
}

const log = logger({
  label: "UIOverlay",
});

export const useStyles = ({
  drawerWidth
}) => makeStyles((theme: Theme) => 
  createStyles({
    overlayBody: {
      paddingTop: theme.mixins.toolbar.height,
      paddingLeft: `${drawerWidth}px`,
      transition: theme.transitions.create(["padding"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      })
    }
  })
)

const Overlay: React.FunctionComponent<OverlayProps> = (props) => {
  const [drawerWidth, setDrawerWidth] = useState(60);
  const openDrawer$ = new Subject<boolean>();
  const classes = useStyles({
    drawerWidth: drawerWidth
  })();

  useEffect(() => {
    openDrawer$.subscribe((value) => {
      setDrawerWidth(value ? 240 : 60)
    })
  }, [openDrawer$, drawerWidth])

  const overlayColor = props.backgroundColor || "#eeeeee1a"

  return (
    <div id="ui-overlay" className={clsx(classes.overlayBody)}>
      <ShrinkingAppBar
        shrinkWidth={drawerWidth}
        backgroundColor={overlayColor}
      >
        <Typography variant="h6" noWrap>
          Chillevery
        </Typography>
        {props.toolbarActions}
      </ShrinkingAppBar>
      <PersistentDrawer
        miniVariant={true}
        underAppBar={false}
        openWidth={drawerWidth}
        backgroundColor={overlayColor}
        openSubject={openDrawer$}
      >
        {props.sideActions}
      </PersistentDrawer>
      {props.children}
    </div>
  );
};

export default Overlay;
