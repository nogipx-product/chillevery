import React, { useContext } from "react";
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  useTheme,
} from "@material-ui/core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faRandom,
  faDice,
  faStopCircle,
  faRedoAlt
} from "@fortawesome/free-solid-svg-icons";
import { SketchBlocContext, ClientSocketBlocContext } from "../App";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";
import { constants } from "buffer";

export interface SideActionsProps {}

const SideActions: React.FunctionComponent<SideActionsProps> = () => {
  const sketchBloc = useContext(SketchBlocContext);
  const socketBloc = useContext(ClientSocketBlocContext);

  const theme = useTheme();

  const contrast = theme.palette.getContrastText(
    theme.palette.background.default
  );

  return (
    <List
      style={{
        position: "absolute",
        bottom: "16px",
        width: "100%",
      }}
    >
      <SideActionButton
        icon={faDice}
        title="Mutate Sketch"
        color={contrast}
        action={() => {
          const sketch = sketchBloc.mutate();
          socketBloc.emitSketch(sketch);
          sketchBloc.runSketch(sketch);
        }}
      />
      <SideActionButton
        icon={faRedoAlt}
        title="Restart"
        color={contrast}
        action={() => {}}
      />
      <SideActionButton
        icon={faStopCircle}
        title="Stop"
        color={contrast}
        action={() => {
          socketBloc.emitHush();
          sketchBloc.hush();
        }}
      />
    </List>
  );
};

export default SideActions;

export interface SideActionButtonProps {
  icon: IconDefinition;
  title: string;
  action: Function;
  color: string;
}

const SideActionButton: React.FunctionComponent<SideActionButtonProps> = (
  props
) => {
  return (
    <ListItem button alignItems="center" onClick={() => props.action()}>
      <ListItemIcon>
        <FontAwesomeIcon size="lg" icon={props.icon} color={props.color} />
      </ListItemIcon>
      <ListItemText>
        <Typography variant="h6" style={{ color: props.color }}>
          {props.title}
        </Typography>
      </ListItemText>
    </ListItem>
  );
};

export { SideActionButton };
