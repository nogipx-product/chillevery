import { Parser } from "acorn";
import { generate } from "astring";
import { attachComments, makeTraveler } from "astravel";
import logger from "../util/logger.js"

const log = logger({
  label: "HydraMutator"
})

class HydraMutator {
  private initialVector;
  private lastLitX;

  constructor() {
    this.initialVector = [];
  }

  public mutate(text: string, reroll?: boolean): string {
    var comments = [];
    let ast = Parser.parse(text, {
      locations: true,
      onComment: comments,
    });

    // Modify the AST.
    ast = this._transform(ast, {
      reroll: reroll
    });

    // Put the comments back.
    attachComments(ast, comments);

    // Generate JS from AST and set back into CodeMirror editor.
    let mutated = generate(ast, { comments: true });
    return mutated;
  }

  public reset() {
    this.initialVector = [];
  }

  // The options object contains a flag that controls how the
  // Literal to mutate is determined. If reroll is false, we
  // pick one at random. If reroll is true, we use the same field
  // we did last time.
  private _transform(ast, options): acorn.Node {
    // An AST traveler that accumulates a list of Literal nodes.
    let traveler = makeTraveler({
      go: function (node, state) {
        if (node.type === "Literal") {
          state.literalTab.push(node);
        } else if (node.type === "MemberExpression") {
          if (node.property && node.property.type === "Literal") {
            // numeric array subscripts are ineligable
            return;
          }
        } else if (node.type === "CallExpression") {
          if (
            node.callee &&
            node.callee.property &&
            node.callee.property.name &&
            node.callee.property.name !== "out"
          ) {
            state.functionTab.push(node);
          }
        }
        // Call the parent's `go` method
        this.super.go.call(this, node, state);
      },
    });

    let state = {
      literalTab: [],
      functionTab: [],
    };

    traveler.go(ast, state);

    let litCount = state.literalTab.length;
    let funCount = state.functionTab.length;
    if (litCount !== this.initialVector.length) {
      let nextVect = [];
      for (let i = 0; i < litCount; ++i) {
        nextVect.push(state.literalTab[i].value);
      }
      this.initialVector = nextVect;
    }

    let litx = 0;
    if (options && options.reroll) {
      if (this.lastLitX !== undefined) {
        litx = this.lastLitX;
      }
    } else {
      litx = Math.floor(Math.random() * litCount);
      this.lastLitX = litx;
    }

    let modLit = state.literalTab[litx];
    if (modLit) {
      let glitched = this._glitchRelToInit(
        modLit.value,
        this.initialVector[litx]
      );
      let was = modLit.raw;
      modLit.value = glitched;
      modLit.raw = "" + glitched;
      log.debug("Literal: " + litx + " changed from: " + was + " to: " + glitched);
    }
    return ast;
  }

  private _glitchRelToInit(num, initVal) {
    if (initVal === undefined) {
      return this._glitchNumber(num);
    }
    if (initVal === 0) {
      initVal = 0.5;
    }

    let rndVal = Math.round(Math.random() * initVal * 2 * 1000) / 1000;
    return rndVal;
  }

  private _glitchNumber(num) {
    if (num === 0) {
      num = 1;
    }
    let range = num * 2;
    let rndVal = Math.round(Math.random() * range * 1000) / 1000;
    return rndVal;
  }
} //  End of class Mutator.

export default HydraMutator;
