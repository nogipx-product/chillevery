import HydraSynth from "hydra-synth";
import loop from "raf-loop";
import logger from "../util/logger.js";
import { Sketch } from "../domain/Sketch.js";

const log = logger({
  label: "HydraController",
});

export class HydraController {
  private isIos: boolean;
  private precisionValue: string;
  public hydra: HydraSynth;
  public engine: any;
  public canvas: HTMLCanvasElement;

  constructor() {
    this.isIos =
      (/iPad|iPhone|iPod/.test(navigator.platform) ||
        (navigator.platform === "MacIntel" && navigator.maxTouchPoints > 1)) &&
      !window.MSStream;

    this.precisionValue = this.isIos ? "highp" : "mediump";
  }

  launch() {
    try {
      if (this.hydra) {
        let loopCallback = (dt) => {
          this.hydra.tick(dt);
        };
        this.engine = loop(loopCallback.bind(this));
        this.engine.start();
      } else {
        log.error("There is not Hydra instance to launch");
      }
    } catch (e) {
      log.error("Error while launch hydra: " + e);
    }
  }

  stop() {
    this.engine.stop();
  }

  setCanvas(canvas: HTMLCanvasElement) {
    this.hydra = new HydraSynth({
      pb: null,
      canvas: canvas,
      autoLoop: false,
      precision: this.precisionValue,
      makeGlobal: false,
      detectAudio: false,
    });
    this.canvas = this.hydra.canvas;
    // Workaround for glsl module which use hydra globals
    window["src"] = this.hydra.synth.src;
    window["a"] = this.hydra.synth.a;
    this.launch();
  }

  run(sketch: Sketch, sync: boolean = false) {
    // May it should be done with safe evaluation lib
    if (!this.hydra) {
      log.error("Cannot run sketch. Hydra has not launched.");
    } else if (!sketch) {
      log.error("Cannot run sketch. Sketch not provided.");
    } else {
      log.debug("Evaluate hydra: " + sketch.code);
      if (sync) this.hydra.synth.time = 0;
      this.hydra.eval(sketch.code);
    }
  }

  hush() {
    log.debug("Hush");
    this.hydra.hush();
  }
}
