import React, { Fragment, useEffect, useState } from "react";
import "./App.css";
import { Sketch } from "./domain/Sketch";
import { withStyles, useTheme } from "@material-ui/core/styles";
import Overlay from "./component/Overlay";
import HydraCanvas from "./component/hydra/HydraCanvas";
import logger from "./util/logger.js";
import HydraEditor from "./component/hydra/HydraEditor";
import WebFont from "webfontloader";
import { SketchBloc } from "./bloc/SketchBloc";
import SideActions from "./component/SideActions";
import ToolbarActions from "./component/ToolbarActions";
import io from "socket.io-client";
import { ClientSocketBloc } from "./bloc/ClientSocketBloc";
import { Button } from "@material-ui/core";
import { RoomSettings } from "./domain/RoomSettings";

const log = logger({
  label: "ChilleveryApp",
});

WebFont.load({
  google: {
    families: ["Fira Code"],
  },
});

let sampleSketch;

// sampleSketch = new Sketch(
//   "const r = (min = 0, max = 0.832) => Math.random() * (max - min) + min; solid(1, 0.927, 1.725).diff(shape([0.71, 4, 4, 43.243].smooth().fast(0.782), r(0.155, 1.358), 0.16).repeat(24.627, 7.151)).modulateScale(osc(5.098).rotate(r(-0.352, 0.677)), 0.686).add(src(o0).scale(1.683).rotate(.012 * Math.round(r(-3.529, 1.456))).color(r(), r(), r()).modulateRotate(o0, r(0.098, 0.56)).brightness(0.079), 0.398).out();"
// );
// sampleSketch = new Sketch(
//   "const r = (min = 0.428, max = 0.297) => Math.random() * (max - min) + min; solid(1, 0.927, 0.108).diff(shape([0.331, 4, 4, 43.243].smooth().fast(0.263), r(0.056, 0.041), 0.037).repeat(45.437, 3.363)).modulateScale(osc(1.935).rotate(r(-0.376, 0.874)), 0.091).add(src(o0).scale(1.683).rotate(0.003 * Math.round(r(-0.389, 1.801))).color(r(), r(), r()).modulateRotate(o0, r(0.085, 0.4)).brightness(0.152), 0.143).out();"
// );
sampleSketch = new Sketch(
  "osc(20, 0.01, 1.1).kaleid(5).color(2.83, 0.91, 0.39).rotate(0, 0.1).scale(1.851).out(o0);"
);
sampleSketch = new Sketch(
  "shape(200, 0.723, 0.47).scale(0.214, 0.5).color([0.819, 1.436].smooth(0.339), 0.126, 0.917).repeat(2, 5.061).modulateScale(osc(5.905, 0.5), -0.945).add(o0, 0.722).scale(0.74).out();"
);

const sketchBloc = new SketchBloc({
  initialSketch: sampleSketch,
});

const socket = io("http://192.168.1.2:3001");
const clientBloc = new ClientSocketBloc(socket);

clientBloc.room$.subscribe((room: RoomSettings) => {
  sketchBloc.runSketch(room.sketch);
});

clientBloc.hush$.subscribe((data) => {
  sketchBloc.hush()
})

export const ClientSocketBlocContext = React.createContext(clientBloc);
export const SketchBlocContext = React.createContext(sketchBloc);

function App() {
  const theme = useTheme();
  const overlayColor = theme.palette.background.default;

  let hydraEditor;

  useEffect(() => {
    sketchBloc.sketch$.subscribe((value) => {});
  }, [sketchBloc]);


  return (
    <SketchBlocContext.Provider value={sketchBloc}>
      <Fragment>
        <Overlay
          backgroundColor={overlayColor}
          sideActions={<SideActions />}
          toolbarActions={<ToolbarActions />}
        >
          <HydraEditor
            theme="the-matrix"
            onInit={(editor) => {
              hydraEditor = editor;
              sketchBloc.sketch$.subscribe((sketch) => {
                if (sketch) editor.setValue(sketch.code);
              });
            }}
            onSave={(code) => {
              const sketch = new Sketch(code);
              clientBloc.emitSketch(sketch);
            }}
          />
          <Button
            variant="contained"
            style={{ zIndex:100, position: "absolute", bottom: 0 }}
            onClick={() => {
              const sketch = new Sketch(hydraEditor.getValue());
              clientBloc.emitSketch(sketch);
              sketchBloc.runSketch(sketch)
            }}
          >
            Run sketch
          </Button>
        </Overlay>
        <HydraCanvas
          qualityCoefficient={1}
          onCanvasReady={(canvas) => {
            sketchBloc.hydra.setCanvas(canvas);
            sketchBloc.runSketch(sketchBloc.currentSketch);
          }}
        />
      </Fragment>
    </SketchBlocContext.Provider>
  );
}

export default App;
