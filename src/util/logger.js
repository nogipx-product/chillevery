const { createLogger, format, transports } = require("winston");

const myFormat = format.printf(({ level, message, label }) => {
  return `[${label}] ${level}: ${message}`;
});

module.exports = ({ label = "Logger", service = "Default" }) => {
  return createLogger({
    defaultMeta: { service: service },
    transports: [
      new transports.Console({
        level: "debug",
        format: format.combine(
          format.colorize(),
          format.label({ label: label }),
          myFormat
        ),
      }),
    ],
  });
};
