const path = require("path");
const fs = require("fs");

export function excludeDirInSrc(dirnames) {
  const scrDir = path.resolve(__dirname, "src");
  if (!Array.isArray(dirnames)) {
    throw "dirnames to exclude should be in array";
  }

  const paths = fs
    .readdirSync(scrDir)
    .filter((file) => !dirnames.includes(file))
    .map((file) => {
      const value = path.resolve(scrDir, file);
      let [filename, ext] = file.split(".");
      filename = fs.lstatSync(value).isDirectory()
        ? filename + "Dir"
        : filename + "File";

      filename = `${filename}${ext ? "_" + ext : ""}`;
      filename = filename.split("-").join("_");

      const entry = {};
      entry[filename] = value;
      return entry;
    })
    .reduce((acc, cur) => Object.assign(acc, cur), {});

  return paths;
}
