import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import logger from "./util/logger";
import * as serviceWorker from "./serviceWorker";
import { ThemeProvider } from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";
import purple from "@material-ui/core/colors/purple";
import green from "@material-ui/core/colors/green";

import "codemirror/lib/codemirror.css";
import "codemirror/theme/monokai.css";
import "codemirror/theme/tomorrow-night-bright.css";
import "codemirror/theme/material.css";
import "codemirror/theme/darcula.css";
import "codemirror/theme/erlang-dark.css";
import "codemirror/theme/midnight.css";
import "codemirror/theme/the-matrix.css";

import "codemirror/mode/javascript/javascript";
import "codemirror/addon/hint/javascript-hint";
import "codemirror/addon/hint/show-hint";
import "codemirror/addon/selection/mark-selection";
import "codemirror/addon/comment/comment";

const log = logger({
  label: "Entrypoint",
});

export const ChilleveryFingerprintSeed = "nogipx"

const theme = createMuiTheme({
  palette: {
    primary: purple,
    secondary: green,
    background: {
      default: "#00000080",
    },
  },
  mixins: {
    toolbar: {
      height: 56,
    },
  },
});



ReactDOM.render(
  <ThemeProvider theme={theme}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </ThemeProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();