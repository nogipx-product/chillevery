import { HydraController } from "../controller/HydraController";
import { Observable, BehaviorSubject } from "rxjs";
import { Sketch } from "../domain/Sketch";
import HydraMutator from "../controller/HydraMutator";
import logger from "../util/logger";

const log = logger({
  label: "SketchBloc",
});

export class SketchBloc {
  private sketch: Sketch;
  public get currentSketch(): Sketch {
    return this.sketch;
  }

  public hydra: HydraController;
  public mutator: HydraMutator;

  private sketchSubject: BehaviorSubject<Sketch>;
  public get sketch$(): Observable<Sketch> {
    return this.sketchSubject.asObservable();
  }

  constructor({ initialSketch }: { initialSketch?: Sketch }) {
    this.sketch = initialSketch;
    this.hydra = new HydraController();
    this.mutator = new HydraMutator();
    this.sketchSubject = new BehaviorSubject<Sketch>(initialSketch);
  }

  public runSketch(sketch: Sketch, { sync = false } = {}) {
    if (sketch) {
      this.sketch = sketch;
      this.sketchSubject.next(this.sketch);
      this.hydra.run(this.sketch, sync);
    } else {
      log.warn("Prevent running empty sketch");
    }
  }

  public mutate(reset?: boolean): Sketch {
    if (this.sketch && this.sketch.code) {
      if (reset) this.mutator.reset();
      let mutatedCode = this.mutator.mutate(this.sketch.code);
      let mutatedSketch = new Sketch(mutatedCode);
      return mutatedSketch;
    } else {
      log.error("There is not sketch to mutate.");
    }
  }

  public hush() {
    this.hydra.hush();
  }

  public dispose() {
    if (this.sketchSubject) this.sketchSubject.complete();
  }
}
