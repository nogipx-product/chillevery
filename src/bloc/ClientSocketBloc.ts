import logger from "../util/logger";
import { Socket } from "socket.io-client";
import { ChilleveryFingerprintSeed } from "..";
import Event from "../domain/SocketEvent";
import { Observable, BehaviorSubject, Subject } from "rxjs";
import { DeviceFingerprint } from "../domain/DeviceFingerprint";
import { RoomSettings } from "../domain/RoomSettings";
import { Sketch } from "../domain/Sketch";

const log = logger({
  label: "ClientSocketBloc",
});

const remote = logger({
  label: "Socket Server",
});

export class ClientSocketBloc {
  private room: RoomSettings;
  private roomSubject: Subject<RoomSettings>;
  get room$() {
    return this.roomSubject.asObservable();
  }
  get currentRoom() {
    return this.room;
  }

  private hushSubject: Subject<boolean>;
  get hush$() {
    return this.hushSubject.asObservable();
  }

  constructor(private socket: SocketIOClient.Socket) {
    this.roomSubject = new Subject<RoomSettings>();
    this.hushSubject = new Subject<boolean>();
    socket.on("connect", () => {
      socket.on("message", (msg: string) => remote.info(msg));

      this.emitFingerprint();
      socket.on(Event.query.device_fingerprint, (room: RoomSettings) => {
        socket.emit(Event.query.room_join, room.id);
      });
      socket.on(Event.query.room_join, (data) => this._updateRoom(data))
      socket.on(Event.response.room_update, (data) => this._updateRoom(data));
      socket.on(Event.request.sketch_hush, () => this._hushRoom())
    });
  }

  private _updateRoom(room: RoomSettings) {
    this.room = room;
    this.roomSubject.next(room);
  }

  private _hushRoom() {
    this.hushSubject.next(false)
  }

  emitSketch(sketch: Sketch) {
    this.room.sketch = sketch;
    this.socket.emit(Event.response.room_update, this.room);
  }

  emitHush() {
    this.socket.emit(Event.request.sketch_hush, this.room);
  }

  async emitFingerprint() {
    DeviceFingerprint.create(ChilleveryFingerprintSeed).then((fp) => {
      fp.hash = "50146d5e17ce2b56b938784c117963b0";
      this.socket.emit(Event.query.device_fingerprint, fp);
    });
  }
}
